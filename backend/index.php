<?php
	header("Access-Control-Allow-Origin: *");
	$connection = mysqli_connect('localhost:3306','root','root','polling');
	mysqli_set_charset($connection, 'UTF8');
	$request_method = $_SERVER["REQUEST_METHOD"];
	switch($request_method)
	{
    case "GET":
		$action = $_GET["action"];
		if ($action == "record") {
			return list_record();
		} else if($action == "vote"){
			return add_polling($_GET["q"], $_GET["a"]);
		} else {
			header("HTTP/1.0 404 Not Found API Call");
			break;
		}
    case "POST":
			header("HTTP/1.0 404 Not Found API Call");
	  	break;
    default:
      header("HTTP/1.0 405 Method Not Allowed");
      break;
  }

  function list_record() {
    global $connection;
    $query = "SELECT * FROM `answers` ORDER BY `id`";
    $result = mysqli_query($connection, $query);
    if ($result){
			$response = $total = $list = array();
      while($row = mysqli_fetch_assoc($result)) {
				$response[] = $row;
	  	}
			foreach ($response as $value) {
				if (isset($total[$value['question_id']])) {
					$total[$value['question_id']] += $value['amount'];
				} else {
					$total[$value['question_id']] = $value['amount'];
				}
				$list[$value['id']] = $value['amount'];
			}
      echo json_encode(
				array(
					"status" => "success",
					"message" => "",
					"data" => array(
						'total' => $total,
						'list' => $list,
					),
				)
			);
    } else {
      echo json_encode(
				array(
					"status" => "failed",
					"message" => "No Records",
				)
			);
    }
  }

	function add_polling($q, $a) {
		global $connection;
		$query = "UPDATE `answers` SET amount = amount + 1 WHERE `question_id` = '".$q."' AND `id` = '".$a."'";
		$result = mysqli_query($connection, $query);
		if ($result){
      echo json_encode(
				array(
					"status" => "success",
					"message" => "Success: Redirect to Index after 5 seconds",
				)
			);
    } else {
      echo json_encode(
				array(
					"status" => "failed",
					"message" => "FAILED",
				)
			);
    }
	}

  mysqli_close($connection);
;?>
