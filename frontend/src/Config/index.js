let ApiKey;

if (process.env.NODE_ENV === 'development') {
  ApiKey = 'http://localhost:8000/';
} else if (process.env.NODE_ENV === 'production') {
  ApiKey = ''; //add your hosting url before build the project
}

export { ApiKey };
