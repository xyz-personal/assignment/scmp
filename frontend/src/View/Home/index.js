import React, { Component } from 'react';
import { Row, Col } from 'antd';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import poll from './../poll.json';
import Charts from './../../Component/Charts';
import { startLoading } from './../../Reducer/Loading/action';
import { getRecords } from './../../Reducer/Records/action';

function mapDispatchToProps(dispatch) {
  return ({
    startLoading: bindActionCreators(startLoading, dispatch),
    getRecords: bindActionCreators(getRecords, dispatch),
  })
}

function mapStateToProps(state) {
  return ({
    data: state.records,
  });
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: ['darkorange', 'navy', 'darkred', 'darkkhaki', 'seagreen', 'indigo'],
      total: {},
      list: {},
    };
    this.chartData = this.chartData.bind(this);
  }

  async componentDidMount() {
    this.props.startLoading(true);
    await this.props.getRecords();
    this.props.startLoading(false);
  }

  componentWillReceiveProps(nextProps) {
    if (typeof (nextProps.data) !== 'undefined') {
      this.setState({ total: nextProps.data.total, list: nextProps.data.list });
    }
  }

  chartData(id, data) {
    const { color, total, list } = this.state;
    const chartData = { title: [], color: [], value: [] };
    if (total.hasOwnProperty(id)) {
      data.map((x, i) => {
        chartData.title.push(x.label);
        chartData.color.push(color[i]);
        chartData.value.push(Number(list[x.id]));
        return;
      })
    }
    return chartData;
  }

  render() {
    const { color, total } = this.state
    return (
      <div id="home" className="container">
        <Row>
          {
            poll.polls.map((x, i) => {
              if (i) {
                return (
                  <Col key={x.id} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }} className="polling-box">
                    <Link to={`/polling/${x.id}`}>
                      <Row style={{ padding: '5px 10px' }}>
                        <Col span={6}>
                          <Charts identity={x.id} height={250} chartData={this.chartData(x.id, x.answer.options)}/>
                        </Col>
                        <Col span={18}>
                          <span className="blue">{moment.unix(x.publishedDate).format("D MMM YYYY")}</span><br />
                          {x.title}
                        </Col>
                      </Row>
                    </Link>
                  </Col>
                );
              } else {
                return (
                  <Col span={24} key={x.id}>
                    <Link to={`/polling/${x.id}`}>
                      <div className="updated-polling blue">
                        <div className="logo" />
                        <span className="title blue">Today's Poll</span>
                        <Row style={{ padding: '5px 10px' }}>
                          <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 16 }} lg={{ span: 16 }}>
                            {x.title}<br />
                            <span className="blue">{moment.unix(x.publishedDate).format("D MMM YYYY")}</span>
                            <div style={{ padding: '20px 0px 20px 10px' }}>
                              {
                                x.answer.options.map((y, j) => (
                                  <div style={{ padding: '5px 0px' }} key={y.label}>
                                    <button disabled style={{ backgroundColor: color[j] }}>
                                      {y.label}
                                    </button>
                                  </div>
                                ))
                              }
                            </div>
                          </Col>
                          <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} style={{ textAlign: 'center' }}>
                            <Charts identity={x.id} height={250} chartData={this.chartData(x.id, x.answer.options)}/>
                          </Col>
                        </Row>
                        <div>
                          Total number of votes recorded: { total.hasOwnProperty(x.id) ? total[x.id] : 0 }
                        </div>
                        <hr /><br />
                      </div>
                      <br />
                    </Link>
                  </Col>
                );
              }
            })
          }
        </Row>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
