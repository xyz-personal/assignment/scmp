import React, { Component } from 'react';
import { Row, Col } from 'antd';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import poll from './../poll.json';
import Charts from './../../Component/Charts';
import { startLoading } from './../../Reducer/Loading/action';
import { getRecords, updateRecords } from './../../Reducer/Records/action';

function mapDispatchToProps(dispatch) {
  return ({
    startLoading: bindActionCreators(startLoading, dispatch),
    getRecords: bindActionCreators(getRecords, dispatch),
    updateRecords: bindActionCreators(updateRecords, dispatch),
  })
}

function mapStateToProps(state) {
  return ({
    data: state.records,
  });
}

class Polling extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      color: ['darkorange', 'navy', 'darkred', 'darkkhaki', 'seagreen', 'indigo'],
      total: {},
      list: {},
      voted: false
    };
    this.chartData = this.chartData.bind(this);
    this.handleVote = this.handleVote.bind(this);
  }

  async componentDidMount() {
    this.props.startLoading(true);
    await this.props.getRecords();
    this.props.startLoading(false);
  }

  componentWillReceiveProps(nextProps) {
    if (typeof (nextProps.data) !== 'undefined') {
      this.setState({ total: nextProps.data.total, list: nextProps.data.list });
    }
  }

  chartData(id, data) {
    const { color, total, list } = this.state;
    const chartData = { title: [], color: [], value: [] };
    if (total.hasOwnProperty(id)) {
      data.map((x, i) => {
        chartData.title.push(x.label);
        chartData.color.push(color[i]);
        chartData.value.push(Number(list[x.id]));
        return;
      })
    }
    return chartData;
  }

  async handleVote(question_id, answer_id) {
    this.props.startLoading(true);
    await this.props.updateRecords(question_id, answer_id);
    this.props.startLoading(false);
    this.setState({ voted: true }); //  lock the button after submit;
    setTimeout(() => {
      this.props.history.push('/'); //  redirection
    }, 5000);
  }

  render() {
    const { id, color, total, voted } = this.state
    const chartID = (data) => { //  generate an uni-key to rerender the chart after submited the vote
      const { total, list } = this.state;
      let string = total.hasOwnProperty(data.id) ? total[data.id] : 0;
      data.answer.options.forEach(x => {
        string += list.hasOwnProperty(x.id) ? `_${list[x.id]}`: '_0';
      })
      return string;
    }
    return (
      <div className="container">
        <Row>
          {
            poll.polls.map((x, i) => {
              if (x.id === Number(id)) {
                return (
                  <Col span={24} key={x.id}>
                    <br />
                    <h1 style={{ paddingLeft: 10 }}>{x.title}</h1><hr />
                    <div style={{ width: '100%' }}>
                      <span style={{ float: 'right' }} >
                        PUBLISHED: {moment.unix(x.publishedDate).format("dddd, D MMMM, YYYY, h:mm a")}&nbsp;&nbsp;&nbsp;
                      </span>
                    </div>
                    <br /><br />
                    <div className="updated-polling" style={{ backgroundColor: 'lightblue' }}>
                      <Row style={{ padding: '5px 20px' }}>
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }}>
                          <div style={{ padding: '20px 0px 20px 10px' }}>
                            {
                              x.answer.options.map((y, j) => (
                                <div style={{ padding: '5px 0px' }} key={y.label}>
                                  <button
                                    disabled={voted}
                                    style={{ backgroundColor: color[j] }}
                                    onClick={() => this.handleVote(x.id, y.id)}
                                  >
                                    {y.label}
                                  </button>
                                </div>
                              ))
                            }
                          </div>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }} style={{ textAlign: 'center' }}>
                          <Charts identity={chartID(x)} height={250} chartData={this.chartData(x.id, x.answer.options)}/>
                        </Col>
                      </Row>
                      <div style={{ padding: '5px 20px' }} className="wording">
                        Total number of votes recorded: { total.hasOwnProperty(x.id) ? total[x.id] : 0 }
                      </div>
                    </div>
                    <br />
                  </Col>
                );
              }
            })
          }
        </Row>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Polling);
