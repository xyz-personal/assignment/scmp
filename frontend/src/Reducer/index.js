import { combineReducers } from 'redux';

import loading from './Loading';
import records from './Records';

const reduxData = combineReducers({
  loading,
  records,
});

export default reduxData;
