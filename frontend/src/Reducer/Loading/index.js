import * as type from './../type';

export default function loading(
  state = false, action,
) {
  switch (action.type) {
    case type.LOADING: {
      return action.data;
    }
    default:
      return state;
  }
}
