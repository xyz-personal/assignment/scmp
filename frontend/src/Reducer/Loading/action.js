import * as type from './../type';

export function startLoading(deter) {
  return (dispatch) => {
    dispatch({
      type: type.LOADING,
      data: deter,
    });
  }
}
