import React, { Component } from 'react';
import { Spin } from 'antd';
import { connect } from 'react-redux';

function mapStateToProps(state) {
  return ({
    loading: state.loading,
  });
}

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (typeof (nextProps.loading) !== 'undefined') {
      this.setState({ loading: nextProps.loading });
    }
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return(
        <div id="loading">
          <div className="loading-box">
            <Spin size="large" />
          </div>
        </div>
      );
    } return (null);
  }
}

export default connect(mapStateToProps, null)(Loading);
