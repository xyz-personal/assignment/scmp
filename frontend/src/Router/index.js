import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './../View/Home';
import Polling from './../View/Polling';
// import Result from './../View/Result';

class MyRouter extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <BrowserRouter basename={'/'}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/polling/:id" component={Polling} />
          {/* <Route exact path="/result" component={Result} /> */}
          {/* <Route component={NoMatch} /> */}
        </Switch>
      </BrowserRouter>
    );
  }
}

export default MyRouter;
