import service from './Request';

export function sendRequest(para) {
  return service({
    url: para,
    method: 'GET',
  });
}
